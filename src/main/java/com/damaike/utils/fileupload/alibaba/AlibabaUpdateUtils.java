package com.damaike.utils.fileupload.alibaba;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import com.aliyun.oss.HttpMethod;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.GeneratePresignedUrlRequest;
import com.aliyun.oss.model.GetObjectRequest;
import com.damaike.utils.fileupload.UpdateUtils;
import com.damaike.utils.fileupload.utils.PropertieUtils;

public class AlibabaUpdateUtils implements UpdateUtils{
	
	
	static String endpoint = null,accessKeyId = null,accessKeySecret = null,bucketName=null;
	
	
	String keyPrefix;
	
	String controlKeyPrefix;
	
	String gridKeyPrefix;
	

	public String getKeyPrefix() {
		if(keyPrefix==null)
			try {
				keyPrefix = PropertieUtils.getProperty("aliyun.oss.key");
			} catch (IOException e) {e.printStackTrace();}
		return keyPrefix;
	}

	public void setKeyPrefix(String keyPrefix) {
		this.keyPrefix = keyPrefix;
	}

	public String getGridKeyPrefix() {
		if(gridKeyPrefix==null)
			try {
				gridKeyPrefix = PropertieUtils.getProperty("aliyun.oss.grid");
			} catch (IOException e) {e.printStackTrace();}
		return gridKeyPrefix;
	}

	public void setGridKeyPrefix(String gridKeyPrefix) {
		this.gridKeyPrefix = gridKeyPrefix;
	}
	
	public String getControlKeyPrefix() {
		if(controlKeyPrefix==null)
			try {
				controlKeyPrefix = PropertieUtils.getProperty("aliyun.oss.control");
			} catch (IOException e) {e.printStackTrace();}
		return controlKeyPrefix;
	}

	public void setControlKeyPrefix(String controlKeyPrefix) {
		this.controlKeyPrefix = controlKeyPrefix;
	}
	
	static{
		try {
			endpoint = PropertieUtils.getProperty("aliyun.oss.endpoint");
			accessKeyId = PropertieUtils.getProperty("aliyun.oss.accessKeyId");
		    accessKeySecret = PropertieUtils.getProperty("aliyun.oss.accessKeySecret");
		    bucketName =  PropertieUtils.getProperty("aliyun.oss.bucketName");
		    

		} catch (IOException e) {e.printStackTrace();}
	}
	
	public static void main(String[] args) throws MalformedURLException, IOException {
		UpdateUtils u = new AlibabaUpdateUtils();
			InputStream inputStream = new FileInputStream(new File("/Users/fufeijian/Downloads/1.jpg"));

			u.update(new String[]{1+".jpg"}, new InputStream[]{inputStream});
		
		URL imageURL = u.geturl("1.jpg", "100", "100");
		System.out.println(imageURL);
	}


	@Override
	public List<String> update(String[] filenames, InputStream[] inputStreams) {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		

		List<String> fileNameList = new ArrayList<String>();
		for(int i =0;i<filenames.length;i++){
			String fullfileName  = getKeyPrefix() +filenames[i];
			 ossClient.putObject(bucketName, fullfileName, inputStreams[i]);
			 fileNameList.add(fullfileName);
		}
		ossClient.shutdown();
		return fileNameList;
	}
	
	@Override
	public URL geturl_pdf(String filename) {
		 //默认 过期时间10分钟
		return geturl_pdf(filename, 10);
	}
	
	@Override
	public URL geturl_pdf(String filename,long delay) {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
	    
	    Date expiration = new Date(new Date().getTime() + 1000 * 60 * delay );
	    GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, getKeyPrefix()+filename, HttpMethod.GET);
	    req.setExpiration(expiration);
	    URL signedUrl = ossClient.generatePresignedUrl(req);
		ossClient.shutdown();
		return signedUrl;
	}
	
	@Override
	public URL geturl(String filename,String w,String h) {
		
		return geturl(filename, w, h, 10);
	}
	
	@Override
	public URL geturl(String filename,String w,String h,long delay) {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		   // 图片处理样式
		if(StringUtils.isEmpty(w))
			w= "100";
		if(StringUtils.isEmpty(h))
			h= "100";
	    String style = "image/resize,m_fixed,w_"+w+",h_"+h;
	    // 过期时间10分钟
	    
	    Date expiration = new Date(new Date().getTime() + 1000 * 60 * delay );
	    GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, getKeyPrefix()+filename, HttpMethod.GET);
	    req.setExpiration(expiration);
	    req.setProcess(style);
	    URL signedUrl = ossClient.generatePresignedUrl(req);
		ossClient.shutdown();
		return signedUrl;
	}

	@Override
	public String downLoadFile(String filename,String saveFilePath) throws IOException {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

		
		// 下载object到文件
		ossClient.getObject(new GetObjectRequest(bucketName, getKeyPrefix()+filename), new File(saveFilePath+filename));
		// 关闭client
		ossClient.shutdown();
		
		return saveFilePath+filename;
	}
	
	@Override
	public URL getControlurl_pdf(String filename) {
		 //默认 过期时间10分钟
		return getControlurl_pdf(filename, 10);
	}
	
	@Override
	public URL getControlurl_pdf(String filename,long delay) {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
	    
	    Date expiration = new Date(new Date().getTime() + 1000 * 60 * delay );
	    GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, getControlKeyPrefix()+filename, HttpMethod.GET);
	    req.setExpiration(expiration);
	    URL signedUrl = ossClient.generatePresignedUrl(req);
		ossClient.shutdown();
		return signedUrl;
	}
	
	@Override
	public URL getControlurl(String filename,String w,String h) {
		
		return getControlurl(filename, w, h, 10);
	}
	
	@Override
	public URL getControlurl(String filename,String w,String h,long delay) {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		   // 图片处理样式
		if(StringUtils.isEmpty(w))
			w= "100";
		if(StringUtils.isEmpty(h))
			h= "100";
	    String style = "image/resize,m_fixed,w_"+w+",h_"+h;
	    // 过期时间10分钟
	    
	    Date expiration = new Date(new Date().getTime() + 1000 * 60 * delay );
	    GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, getControlKeyPrefix()+filename, HttpMethod.GET);
	    req.setExpiration(expiration);
	    req.setProcess(style);
	    URL signedUrl = ossClient.generatePresignedUrl(req);
		ossClient.shutdown();
		return signedUrl;
	}

	@Override
	public String downLoadControlFile(String filename,String saveFilePath) throws IOException {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

		
		// 下载object到文件
		ossClient.getObject(new GetObjectRequest(bucketName, getControlKeyPrefix()+filename), new File(saveFilePath+filename));
		// 关闭client
		ossClient.shutdown();
		
		return saveFilePath+filename;
	}

	@Override
	public URL getGridurl_pdf(String filename) {
		 //默认 过期时间10分钟
		return getGridurl_pdf(filename, 10);
	}
	
	@Override
	public URL getGridurl_pdf(String filename,long delay) {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
	    
	    Date expiration = new Date(new Date().getTime() + 1000 * 60 * delay );
	    GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, getGridKeyPrefix()+filename, HttpMethod.GET);
	    req.setExpiration(expiration);
	    URL signedUrl = ossClient.generatePresignedUrl(req);
		ossClient.shutdown();
		return signedUrl;
	}
	
	@Override
	public URL getGridurl(String filename,String w,String h) {
		
		return getGridurl(filename, w, h, 10);
	}
	
	@Override
	public URL getGridurl(String filename,String w,String h,long delay) {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		   // 图片处理样式
		if(StringUtils.isEmpty(w))
			w= "100";
		if(StringUtils.isEmpty(h))
			h= "100";
	    String style = "image/resize,m_fixed,w_"+w+",h_"+h;
	    // 过期时间10分钟
	    
	    Date expiration = new Date(new Date().getTime() + 1000 * 60 * delay );
	    GeneratePresignedUrlRequest req = new GeneratePresignedUrlRequest(bucketName, getGridKeyPrefix()+filename, HttpMethod.GET);
	    req.setExpiration(expiration);
	    req.setProcess(style);
	    URL signedUrl = ossClient.generatePresignedUrl(req);
		ossClient.shutdown();
		return signedUrl;
	}

	@Override
	public String downLoadGridFile(String filename,String saveFilePath) throws IOException {
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

		
		// 下载object到文件
		ossClient.getObject(new GetObjectRequest(bucketName, getGridKeyPrefix()+filename), new File(saveFilePath+filename));
		// 关闭client
		ossClient.shutdown();
		
		return saveFilePath+filename;
	}




	
	
}
