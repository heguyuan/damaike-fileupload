package com.damaike.utils.fileupload.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertieUtils {
	public static String getProperty(String key) throws IOException {
		InputStream inputStream;      
		ClassLoader cl = PropertieUtils.class.getClassLoader();      
		if  (cl !=  null ) {      
		         inputStream = cl.getResourceAsStream( "upload.properties" );      
		}  else {      
		         inputStream = ClassLoader.getSystemResourceAsStream("upload.properties" );      
		}      
		Properties properties =  new  Properties();      
		properties.load(inputStream);      
		inputStream.close(); 
		String value = properties.getProperty(key);
		if(value!=null)
			return properties.getProperty(key);
		else 
			return null;
	}
}
