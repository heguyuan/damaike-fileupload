package com.damaike.utils.fileupload;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

public interface UpdateUtils {
	//返回完整文件名
	public List<String> update(String[] filenames,InputStream[] inputStreams);
	
	public URL  geturl(String filename,String w,String h);
	public URL geturl(String filename,String w,String h,long delay) ;
	
	

	String downLoadFile(String filename, String saveFilePath) throws IOException;

	URL geturl_pdf(String filename);

	URL geturl_pdf(String filename, long delay);

	URL getControlurl(String filename, String w, String h);

	URL getControlurl_pdf(String filename);

	URL getControlurl_pdf(String filename, long delay);

	URL getControlurl(String filename, String w, String h, long delay);

	String downLoadControlFile(String filename, String saveFilePath) throws IOException;

	URL getGridurl_pdf(String filename);

	URL getGridurl_pdf(String filename, long delay);

	URL getGridurl(String filename, String w, String h);

	URL getGridurl(String filename, String w, String h, long delay);

	String downLoadGridFile(String filename, String saveFilePath) throws IOException;
}
